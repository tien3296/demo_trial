terraform {
  backend "s3" {
    bucket = "remote-state111"
    key    = "remote-state-main"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region = var.provider_region
}

resource "aws_autoscaling_group" "myASG"{
    name= var.ASGname
    desired_capacity = var.desired_capacity
    min_size = var.min_size
    max_size= var.max_size
    
    launch_template{
        id= aws_launch_template.foobar.id
        version = "$Latest"
    }
    vpc_zone_identifier= var.vpc_zone_identifier
}

resource "aws_launch_template" "foobar" {
  name_prefix   = var.name-prefix
  image_id      = var.image_id
  instance_type = "t2.micro"
  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "main"
    }
  }
}


