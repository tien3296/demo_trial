variable "bucket" {
  type = string

}
variable "key" {
  type = string
  
}
variable "remotebackend-region" {
  type = string
  
  
}
variable "provider_region" {
    type = string
    
  
}
variable "ASGname" {
    type = string
    
}
variable "desired_capacity" {
    type = number
    
}
variable "min_size" {
    type = number
    
}
variable "max_size" {
    type = number
    
}

variable "vpc_zone_identifier" {
    type = list(string)
    
    
}
variable "name-prefix" {
    type = string
    
}

variable "image_id" {
    type = string
    
}




variable "instance_type" {
    type = string
    
}

variable "name_ec2_ASG" {
    type = string
    
  
}